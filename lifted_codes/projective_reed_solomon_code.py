# -*- Python -*-
# -*- coding: utf-8 -*-

# Sage imports
from sage.coding.linear_code import AbstractLinearCode
from sage.coding.encoder import Encoder
from sage.coding.decoder import Decoder
from sage.matrix.constructor import matrix
from sage.schemes.projective.projective_space import ProjectiveSpace
from sage.rings.finite_rings.finite_field_constructor import GF
from sage.modules.free_module_element import vector
# Python imports
from copy import copy

# Local imports
from monomial_code import *


# --------------------------------------------------------------------------- #
class ProjectiveReedSolomonCode(MonomialCode):
    r"""

    EXAMPLES::

        sage: from projective_reed_solomon_code import *
        sage: P = ProjectiveReedSolomonCode(4, 2)
        sage: P
        [5, 3, 3] projective Reed-Solomon code over GF(4)
    """
    def __init__(self, q, k):
        if k > q:
            raise ValueError("k must be between 0 and q")
        FF = GF(q)
        D = DegreeSet(2, q, [tuple([i, k-i]) for i in range(k+1)])
        super(ProjectiveReedSolomonCode, self).__init__(FF, D, True)
        self._k = k        


    def _repr_(self):
        r"""
        TESTS::

            sage: from projective_reed_solomon_code import *
            sage: P = ProjectiveReedSolomonCode(11, 7)
            sage: repr(P)
            '[12, 8, 5] projective Reed-Solomon code over GF(11)'
        """
        return ("[%d, %d, %d] projective Reed-Solomon code over GF(%d)"
                % (self.length(), self.dimension(), self.minimum_distance(),
                   self.base_field().cardinality() ))

    
    def _latex_(self):
        r"""
        TESTS::

            sage: from projective_reed_solomon_code import *
            sage: P = ProjectiveReedSolomonCode(11, 7)
            sage: latex(P)
            [12, 8, 5] \textnormal{projective Reed-Solomon code over } \Bold{F}_{11}
        """
        return ("[%d, %d, %d] \\textnormal{projective Reed-Solomon code over } %s"
                % (self.length(), self.dimension(), self.minimum_distance(),
                   self.base_field()._latex_() ))

    def dimension(self):
        r"""
        TESTS::

            sage: from projective_reed_solomon_code import *
            sage: P = ProjectiveReedSolomonCode(16, 13)
            sage: P.dimension()
            14
        """
        return self._k + 1

    def minimum_distance(self):
        r"""
        TESTS::

            sage: from projective_reed_solomon_code import *
            sage: P = ProjectiveReedSolomonCode(16, 13)
            sage: P.minimum_distance()
            4
        """
        return self.length() - self.dimension() + 1

    def evaluation_points(self):
        """
        TO COMMENT
        """
        return ProjectiveSpace(self.base_field(), 1).rational_points()

    
    def parity_check_matrix(self):
        """
        TO COMMENT
        """
        pts = self.evaluation_points()
        L = []
        m = self.length() - self.dimension()
        q = self.base_field().cardinality()
        for i in range(m):
            L.append([1 if i == 0 else 0]
                     + [pts[j][0]**i for j in range(1,q) ]
                     + [1 if i == m-1 else 0])
        return matrix(self.base_field(), L)
# --------------------------------------------------------------------------- #


# --------------------------------------------------------------------------- #
class PRSDecoder(Decoder):
    r"""

    EXAMPLES::

        sage: from projective_reed_solomon_code import *
        sage: q = 9; k = 5
        sage: P = ProjectiveReedSolomonCode(q, k)
        sage: D = PRSDecoder(P)
        sage: D
        Decoder for [10, 6, 5] projective Reed-Solomon code over GF(9)

    A projective Reed-Solomon code puntured at infinity is a full-length 
    Reed-Solomon code::
 
        sage: R = GeneralizedReedSolomonCode(GF(q).list(), k+1)
        sage: LinearCode(R) == LinearCode(P.punctured([q]))
        True
    """
    def __init__(self, code):
        if not isinstance(code, ProjectiveReedSolomonCode):
            raise ValueError("code has to be a projective Reed-Solomon code")
        super(PRSDecoder, self).__init__(code, code.ambient_space(),
            "MonomialEvaluationEncoder")

    def _repr_(self):
        r"""
        TESTS::

            sage: from projective_reed_solomon_code import *
            sage: P = ProjectiveReedSolomonCode(11, 7)
            sage: D = PRSDecoder(P)
            sage: D
            Decoder for [12, 8, 5] projective Reed-Solomon code over GF(11)
        """
        return ("Decoder for %s" % self.code())

    
    def decoding_radius(self):
        """
        TO COMMENT
        """
        return (self.code().minimum_distance() - 1)//2

    def _partial_xgcd(self, a, b, PolRing, stop):
        """
        TO COMMENT
        """
        prev_t = PolRing.zero()
        t = PolRing.one()

        prev_r = a
        r = b

        while(r.degree() >= stop):
            q = prev_r.quo_rem(r)[0]
            prev_r, r = r, prev_r - q * r
            prev_t, t = t, prev_t - q * t

        return (r, t)

    def decode_to_code(self, r):
        r"""
        TESTS::

            sage: from projective_reed_solomon_code import *
            sage: P = ProjectiveReedSolomonCode(11, 7)
            sage: D = PRSDecoder(P)
            sage: c = P.random_element()
            sage: Chan = channels.StaticErrorRateChannel(P.ambient_space(), D.decoding_radius())
            sage: y = Chan(c)
            sage: D.decode_to_code(y) == c
            True
        """
        FF = self.code().base_field()
        q = FF.cardinality()
        R = PolynomialRing(FF, "X")
        X = R.gen()
        d = self.code().minimum_distance()

        syndrome = self.code().parity_check_matrix() * r
        S = R(syndrome.list())
        a = X ** (d - 1)

        (EEP, ELP) = self._partial_xgcd(a, S, R, (d-1)//2)
        ELPd = ELP.derivative()
        pts = self.code().evaluation_points()
        e = [0 for i in range(q+1)]
        for i in range(1, q):
            x = 1/pts[i][0]
            if ELP(x) == 0:
                e[i] = -EEP(x)/(x*ELPd(x))
        e[0] = S[0] - sum(e[1:q])
        e[q] = S[d-2] - sum([ e[i] * (pts[i][0]**(d-2)) for i in range(1,q)])
        
        return r - vector(FF, e)
# --------------------------------------------------------------------------- #
