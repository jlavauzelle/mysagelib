# -*- Python -*-
# -*- coding: utf-8 -*-

from code_operations import *

from sage.coding.linear_code import AbstractLinearCode, \
    LinearCodeGeneratorMatrixEncoder
from sage.misc.cachefunc import cached_method
from sage.rings.integer import Integer
from sage.rings.polynomial.polynomial_ring_constructor import PolynomialRing
from sage.matrix.constructor import matrix
from sage.modules.free_module_element import vector
from sage.groups.perm_gps.permgroup import PermutationGroup
from sage.groups.perm_gps.constructor import PermutationGroupElement
from sage.combinat.permutation import Permutation
from sage.matrix.matrix_space import MatrixSpace
from sage.arith.misc import gcd, xgcd







class DihedralCode(AbstractLinearCode):
    
    _registered_encoders = {}
    _registered_decoders = {}

    def __init__(self, P0, P1, half_length):
        r"""
        Creates an instance of a DihedralCode with the pair of generator
        polynomials ``P0`` and ``P1``, of length 2 times ``half_length``.

        INPUT:
        
        - ``P0`` and ``P1`` -- the pair od generator polynomials of the dihedral code

        - `` half_length`` -- an integer correspondig to half the length of the code
        TESTS::
            sage: from dihedral_code import *
            sage: Fq = GF(2)
            sage: R = PolynomialRing(Fq, "X")
            sage: X = R.gen()
            sage: m = 5
            sage: P0 = X + 1
            sage: P1 = X**2 + X + 1

            sage: DihedralCode(P0, P1, m)
            Dihedral Code of length 10 with generators (X + 1, X^2 + X + 1)

            sage: DihedralCode(P0, P1, m).half_length() == m
            True

            sage: DihedralCode(P0, P1, m).extension_field_material()
            (Vector space of dimension 2 over Finite Field of size 2,
            Isomorphism:
              From: Vector space of dimension 2 over Finite Field of size 2
              To:   Finite Field in z2 of size 2^2,
            Isomorphism:
              From: Finite Field in z2 of size 2^2
              To:   Vector space of dimension 2 over Finite Field of size 2)

            sage: DihedralCode(P0, P1, m).polynomial_rings()
            (Univariate Polynomial Ring in X over Finite Field of size 2 (using GF2X),
            Univariate Polynomial Ring in XX over Finite Field in z2 of size 2^2,
            Univariate Quotient Polynomial Ring in Xbar over Finite Field of size 2 with modulus X^5 + 1)
        """
        if not(isinstance(half_length, (int, Integer))):
            raise ValueError("`half_length` must be an integer. Given: %s" % half_length)
        
        half_length = Integer(half_length)
        Fq = P0.base_ring()
        Fq2 = Fq.extension(2)
        omega = Fq2.multiplicative_generator()

            
        self._m = half_length
        self._length = 2*half_length

        # Corps
        self._Fq = Fq
        self._Fq2 = Fq2
        self._V, self._phi, self._psi = Fq2.vector_space(Fq, map=True)

        # Polynomes et anneaux de polynomes
        self._R = PolynomialRing(Fq, 'X')
        self._Rext = PolynomialRing(Fq2, 'XX')
        self._X = self._R.gen()
        self._XX = self._Rext.gen()
        self._Xmm1 = self._X**self._m - 1        
        self._Rbar = self._R.quo(self._Xmm1, "Xbar")

        self._polyext = self._Rext(P0) + omega * self._Rext(P1)    

        
        self._polymat_space = PolynomialRing(MatrixSpace(Fq, 2, 2), "XX")
        self._matpoly_space = MatrixSpace(self._Rbar, 2, 2)

        self._generators = vector([P0, P1])
        PP = [[P0,P1], [self.polybar(P1), self.polybar(P0)]]
        self._generator_matpoly = matrix(self._R, 2, 2, PP)
        self._reduced_matpoly = None
        self._determinant = self._generator_matpoly.determinant()
        self._xgcd_generators = P0.xgcd(P1)
        
        super(DihedralCode, self).__init__(self._Fq, 2*self._m, "GeneratorMatrix", "Syndrome")


    
    ############################################################
    # USUAL CODING THEORY / SAGE METHODS                       #
    ###

    def __eq__(self, other):
        r"""
        Tests equality between ``self`` and ``other``.
        """
        return LinearCode(self) == LinearCode(other)
    
    def _repr_(self):
        r"""
        Returns a string representation of ``self``.
        """
        return("Dihedral Code of length %d with generators %s" % (self.length(), str(self._generators)))


    

    ############################################################
    # SPECIFIC GETTERS                                         #
    ###

    
    def half_length(self):
        r"""
        Returns half the length of ``self``.
        """
        return self._m

    
    def generator_matpoly(self):
        return self._generator_matpoly
    
    
    def generators(self):
        return self._generators

    
    def determinant(self):
        return self._determinant

    
    def reduced_matpoly(self):
        if self._reduced_matpoly is None:
            # aux = self._trigonalize_2x2(self.generator_matpoly())
            # self._reduced_matpoly = matrix(2, 2, [A % self._Xmm1 for col in aux for A in col])
            return self._fullreduce_2x2_bar(self.generator_matpoly())
        return self._reduced_matpoly


    
    def generator_polyext(self):
        r"""
        Returns the generator polynomial (defined over the quadratic extension)
        which defines ``self``.
        """
        return self._polyext
    
    
    
    def twisted_generator_polyext(self):
        r"""
        Returns the twist of the generator polynomial of ``self``.
        """
        return self.polyext_twist(self.generator_polyext())
    

    def extension_field(self):
        r"""
        Returns the extension field GF(q^2).
        """
        return self._Fq2

    def extension_field_material(self):
        r"""
        Returns useful spaces and maps between GF(q)^2 and GF(q^2).
        """
        return self._V, self._phi, self._psi

    def polynomial_rings(self):
        r"""
        Returns useful polynomial rings.
        """
        return self._R, self._Rext, self._Rbar
    
    def polymat_space(self):
        r"""
        Returns the space Mat(2, 2, GF(q))[X].
        """
        return self._polymat_space
    
    
    def _quasi_cyclic_code(self, poly):
        m = self.half_length()
        Fqm = self.extension_field()
        Fq = self.base_field()
        V, phi, psi = self.extension_field_material()
        vec_poly = list(self.polyext_to_vec(poly))
        L = [ vec_poly[2*(m-i):] + vec_poly[:2*(m-i)] for i in range(m) ]
        G = matrix(Fq, L)
        return LinearCode(G)
        
    @cached_method
    def generator_matrix(self):
        r"""
        TESTS::
            sage: from dihedral_code import *
            sage: Fq = GF(2)
            sage: R = PolynomialRing(Fq, "X")
            sage: X = R.gen()
            sage: m = 5
            sage: P0 = X + 1
            sage: P1 = X**2 + X + 1
            sage: C = DihedralCode(P0, P1, m)
            sage: C.generator_matrix()
            [1 0 0 0 0 0 0 1 0 0]
            [0 1 0 0 0 0 1 1 1 1]
            [0 0 1 0 0 0 0 0 0 1]
            [0 0 0 1 0 0 1 0 0 0]
            [0 0 0 0 1 0 1 1 1 1]
            [0 0 0 0 0 1 0 0 1 0]
        """
        poly = self.generator_polyext()
        m =  self.half_length()
        C1 = self._quasi_cyclic_code(poly)
        poly_bar = self.polyext_twist(poly)
        C2 = self._quasi_cyclic_code(poly_bar)
        return (C1+C2).generator_matrix()
        
    def dimension(self):
        r"""
        TESTS::
            sage: from dihedral_code import *
            sage: Fq = GF(2)
            sage: R = PolynomialRing(Fq, "X")
            sage: X = R.gen()
            sage: m = 5
            sage: P0 = X + 1
            sage: P1 = X**2 + X + 1
            sage: C = DihedralCode(P0, P1, m)
            sage: C.dimension()
            6
        """
        return self.generator_matrix().nrows()


    def dihedral_gens(self):
        m = self.half_length()
        g1 = Permutation( [tuple([2*i+1 for i in range(m)]),tuple([2*i+2 for i in range(m)])] )
        g2 = Permutation([ tuple([1,2]) ] + [ tuple([3+2*i, 2*m-2*i]) for i in range(m-1) ])
        return g1, g2

    def dihedral_permutation_subgroup(self):
        return PermutationGroup(self.dihedral_gens())



    

    ############################################################
    # SWITCH BETWEEN DIFFERENT REPRESENTATIONS OF THE ELEMENTS #
    ###

    def field_twist(self, x):
        r"""
        Twists an element of the quadratic extension field.
        """
        _, phi, psi = self.extension_field_material()
        v = psi(x)
        return phi(vector([v[1], v[0]]))

    
    def polyext_twist(self, poly):
        r"""
        Twists a polynomial defined over the quadratic extension field, according
        to the half-length of the code.
        """
        m = self.half_length()
        coeffs = poly.coefficients(sparse=False)
        coeffs += [0]*(m-len(coeffs))
        new_coeffs = [self.field_twist(coeffs[0])] + [self.field_twist(coeffs[m-i]) for i in range(1, m)]
        X = self.polynomial_rings()[1].gen()
        return sum([new_coeffs[i]*X**i for i in range(m)])

    
    def vec_twist(self, v):
        beta = self.dihedral_gens()[1]
        return vector(beta.action(v))

    
    def polymat_twist(self, M):
        return matrix(self.base_field(), 2, 2, [0, 1, 1, 0]) * M

    
    
    def vec_to_polyext(self, v):
        r"""
        Returns the polynomial over GF(q^2) corresponding to the vector v over GF(q).
        The polynomial is reduced modulo (X^m - 1).
        """
        phi = self.extension_field_material()[1]
        v = [ phi(vector(v[2*i:2*i+2])) for i in range(self.half_length())]
        R = self.polynomial_rings()[1]
        return R(v)
    

    def vec_to_polymat(self, v):
        r"""
        Returns the polynomial over M2(GF(q)) corresponding to the vector v over GF(q).
        """
        MS = self.polymat_space()
        A = MS.base_ring()
        X = MS.gen()
        m = self.half_length()
        return A([v[0], v[1], v[1], v[0]])*X**0 + sum([A([v[2*i+2], v[2*i+3], v[2*m-2*i-1], v[2*m-2*i-2]])*X**(i+1) for i in range(m-1)])
    

    def vec_to_matpoly(self, v):
        r"""
        Returns the (2x2)-matrix over GF(q)[X]/(X^m-1) corresponding to the vector v over GF(q).
        """
        R = self._quoR
        list_coeffs_mat = [ A for A  in self.vec_to_polymat(v)]
        M = []
        for i in range(2):
            for j in range(2):
                M.append(R([A[i][j] for A in list_coeffs_mat]))
        return matrix(2, 2, M)
        
    
    def polyext_to_vec(self, p):
        r"""
        Returns the vector v over GF(q) corresponding to the polynomial p defined
        over GF(q^2).
        """
        if p.is_zero():
            return self.ambient_space().zero()
        psi = self.extension_field_material()[2]
        vec_poly = p.coefficients(sparse=False)
        vec_poly = vec_poly + [self.extension_field().zero()]*(self.half_length() - p.degree()-1)
        v = [ psi(u) for u in vec_poly ]
        return vector([ a for b in v for a in b])

    
    def polyext_to_polymat(self, p):
        r"""
        Returns the polynomial over M2(GF(q)) corresponding to the polynomial p defined
        over GF(q^2).
        """
        v = self.polyext_to_vec(p)
        return self.vec_to_polymat(v)
    
    
    def polymat_to_vec(self, M):
        r"""
        Returns the vector v over GF(q) corresponding to the polynomial over M2(GF(q)).
        """
        list_M = M.coefficients(sparse=False)
        v = []
        for A in list_M:
            v.append(A[0,0])
            v.append(A[0,1])
        v += [0]*(self.length()-len(v))
        return vector(v)
    

    def polymat_to_polyext(self, M):
        r"""
        Returns the polynomial over GF(q^2) related to the polynomial over M2(GF(q)) given
        as an entry.
        """        
        v = self.polymat_to_vec(M)
        return self.vec_to_polyext(v)
    

    def polymat_transpose(self, M):
        r"""
        Returns the polynomial over M2(GF(q)) defined by transposing each coefficients of
        the entry (which is a polynomial M over M2(GF(q)))
        """
        X = M.parent().gen()
        return sum(M[i].transpose() * X**i for i in range(M.degree()+1))


    def polybar(self, P):
        if P == 0:
            return 0
        auxP = P//self._X
        d = auxP.degree()
        return P[0] + (self._X)**(self._m - 1 - d) * auxP.reverse()

    def reciprocal_polynomial(self, P):
        if P == 0:
            return 0
        return self._X**(self._m-1-P.degree()) * P.reverse()

    


    def reciprocal_polymat(self, M):
        return M.reverse(self.half_length()-1)
    
    def mult_polymat(self, Mu, Mv):
        return (Mu*Mv) % self._polymat_quo

    def mult_vec(self, u, v):
        Mu = self.vec_to_polymat(u)
        Mv = self.vec_to_polymat(v)
        return self.polymat_to_vec(self.mult_polymat(Mu, Mv))

    def mult_polyext(self, Pu, Pv):
        Mu = self.polyext_to_polymat(Pu)
        Mv = self.polyext_to_polymat(Pv)
        return self.polymat_to_polyext(self.mult_polymat(Mu, Mv))

    def gcdex(self, A, B):
        G, U, V = xgcd(A, B)
        if A == 0:
            return matrix(A.parent(), [[0, 1], [-1, 0]]), B
        if B == 0:
            return matrix(A.parent(), [[1, 0], [0, 1]]), A
        if A.divides(B):
            return matrix(A.parent(), [[1, 0], [-B//A, 1]]), G
        return matrix([[U, V], [-B//G, A//G]]), G
    
    def _trigonalize_2x2(self, M):     
        r = 0
        for k in range(2):
            for i in range(r+1, 2):
                U, G = self.gcdex(M[r, k], M[i, k])
                M = U * M
            if M[r, k] != 0:
                r += 1
        return M    
    
    def _fullreduce_2x2(self, M):
        M = self._trigonalize_2x2(M)
        print("apres trigo :\n" + str(M))        
        for i in range(2):
            g = gcd(M[i,0], M[i,1])
            M[i,0] //= g
            M[i,1] //= g
            print("g = ", g)
        print("apres trigo 2 :\n" + str(M))  
        # M = matrix(2, 2, [A % self._Xmm1 for col in M for A in col])
        print("apres red mod X^m-1 :\n" + str(M))
        S = M[0,1] % M[1,1]
        M = matrix([[M[0,0], S], [M[1,0], M[1,1]]])
        print("apres red M[0,1] :\n" + str(M))                
        return M



    
    def gcdex_bar(self, Abar, Bbar):
        # on suit la these de storjohann : calcule une matrice inverible M
        # telle que M * [ Abar, Bbar ]^T = [G, 0]^T où G est le
        # "pgcd" entre Abar et Bbar
        phi = lambda U: self._Rbar(U)
        phi_inv = lambda Ubar: self._R(Ubar.list())
        A, B = phi_inv(Abar), phi_inv(Bbar)
        M, G = self.gcdex(A, B)
        return matrix(self._Rbar, 2, 2, [phi(W) for col in M for W in col ]), phi(G)

    def _trigonalize_2x2_bar(self, Mbar):
        r = 0
        for k in range(2):
            i = r+1
            for i in range(r+1, 2):
                U, Gbar = self.gcdex_bar(Mbar[r, k], Mbar[i, k])
                Mbar = U * Mbar
                assert(U.is_invertible())
            if Mbar[r, k] != 0:
                r += 1
        return Mbar

    def _reduce_row_2x2_bar(self, Mbar, r):
        rows = Mbar.rows()
        row = Mbar[r]
        G = gcd([self._R(R.list()) for R in row])
        Gx, U, V = xgcd(G, self._Xmm1)
        row = [ self._Rbar(U)*R for R in row]
        rows[r] = row
        return matrix(rows)
        
    def _fullreduce_2x2_bar(self, Mbar):
        Mbar = self._trigonalize_2x2_bar(Mbar)
        Mbar = self._reduce_row_2x2_bar(Mbar, 1)
        return Mbar

    def prescribed_dimension(self):
        p0, p1 = self._generators[0], self._generators[1] 
        g0  = gcd(p0, self.polybar(p1))
        det =  self.determinant()
        det_g0 = det//g0
        return 2 * self._m - gcd(g0, self._Xmm1).degree() - gcd(det_g0, self._Xmm1).degree()
    
    
DihedralCode._registered_encoders["GeneratorMatrix"] = LinearCodeGeneratorMatrixEncoder
