# -*- Python -*-
# -*- coding: utf-8 -*-

from sage.coding.linear_code import AbstractLinearCode, LinearCode
from sage.matrix.constructor import matrix
from sage.matrix.special import identity_matrix
from sage.rings.integer import Integer
from sage.combinat.permutation import Permutations
from sage.groups.perm_gps.constructor import PermutationGroupElement

def vector_space(self):
    r"""
    Return the code seen over an extension field

    TESTS::
        sage: from generic_code_operations import *
        sage: C = codes.HammingCode(GF(2), 3)
        sage: C.vector_space()
        Vector space of degree 7 and dimension 4 over Finite Field of size 2
        Basis matrix:
        [1 0 0 0 0 1 1]
        [0 1 0 0 1 0 1]
        [0 0 1 0 1 1 0]
        [0 0 0 1 1 1 1]
    """
    G = self.generator_matrix()
    return G.image()

def sum_of_codes(self, other):
    r"""
    Return the code seen over an extension field

    TESTS::
        sage: from generic_code_operations import *
        sage: C = codes.ParityCheckCode(GF(2), 6).dual_code()
        sage: D = codes.HammingCode(GF(2), 3).dual_code()
        sage: C + D == LinearCode(codes.HammingCode(GF(2), 3))
        True
    """
    return LinearCode(self.vector_space() + other.vector_space())
                      
def intersection_of_codes(self, other):
    r"""
    Return the code seen over an extension field

    TESTS::
        sage: from generic_code_operations import *
        sage: C = codes.ParityCheckCode(GF(2), 6)
        sage: D = codes.HammingCode(GF(2), 3)
        sage: C.intersection(D) == LinearCode(codes.HammingCode(GF(2), 3).dual_code())
        True
    """
    return LinearCode(self.vector_space().intersection(other.vector_space()))

def base_extend(self, E):
    r"""
    Return the code seen over an extension field

    TESTS::
        sage: from generic_code_operations import *
        sage: FF, EE = GF(8), GF(64)
        sage: L_FF = GF(8).list()
        sage: L_EE = [ EE(x) for x in L_FF ]
        sage: A = codes.GeneralizedReedSolomonCode(L_EE, 3)
        sage: B = codes.GeneralizedReedSolomonCode(L_FF, 3)
        sage: B.base_extend(EE) == LinearCode(A)
        True
    """
    V = self.vector_space()
    W = V.base_extend(E)
    return LinearCode(W)
    
def concatenate(self, outer):
    F = inner.base_field()
    E = outer.base_field()
    m = E.degree()//F.degree()
    assert(m == inner.dimension())
    G = inner.generator_matrix()
    V, phi, psi = E.vector_space(F, map=True)
    A = [ phi(v) for v in V.basis() ]
    G_concat = matrix([[ u for x in c for u in psi(a*x)*Gd ] for c in G.rows() for a  in A ])
    return LinearCode(G_concat)

def schur_product(self, other, algo="std"):
    r"""
    Return the Schur product of `self` and `other`.

    TESTS::
        sage: from generic_code_operations import *
        sage: A = codes.GeneralizedReedSolomonCode(GF(7).list(), 2)
        sage: B = codes.GeneralizedReedSolomonCode(GF(7).list(), 3)
        sage: C = A.schur_product(B)
        sage: C == LinearCode(codes.GeneralizedReedSolomonCode(GF(7).list(), 4))
        True
    """
    if self.base_ring() != other.base_ring():
        raise TypeError("self and other must have the same base field")
    A = self.generator_matrix()
    B = other.generator_matrix().rref()
    G = matrix(self.base_field(), 0, self.length())
    if (algo == "std"):
        for b in B.rows():
            G = G.stack(matrix([a.pairwise_product(b) for a in A]))
        return LinearCode(G.row_space().matrix())
    if (algo == "elaborate"):
        n = self.length()
        kB = other.dimension()
        G = matrix(self.base_field(), 0, self.length())
        i = 0
        for b in Permutations(kB).random_element().action(B.rows()):
            if G.rank() != n:
                aux = matrix([a.pairwise_product(b) for a in A])
                G = G.stack(aux).rref()
                r = G.rank()
                G = G[:r]
                i += 1
        return LinearCode(G)

def schur_power(self, p):
    r"""
    Return the Schur power of `self`.

    TESTS::
        sage: from generic_code_operations import *
        sage: A = codes.GeneralizedReedSolomonCode(GF(23).list(), 3)
        sage: C = A.schur_power(6)
        sage: C == LinearCode(codes.GeneralizedReedSolomonCode(GF(23).list(), 13))
        True
    """
    res = LinearCode(matrix([self.base_field().one() for i in range(self.length())]))
    B = Integer(p).bits()
    B.reverse()
    for b in B:
        res = res.schur_product(res)
        if b == 1:
            res = res.schur_product(self)    
    return res

def schur_conductor(self, C):
    r"""
    Return the Schur conductor of `self` into `C`, i.e. the set of vectors `b` such
    that `b \star self` is a subspace of `C`.

    TESTS::
        sage: from generic_code_operations import *
        sage: F = GF(23)
        sage: w = F.multiplicative_generator()
        sage: yA = vector([ w**i for i in range(23) ])
        sage: yB =  vector([ w**(3*i) for i in range(23) ])
        sage: A = codes.GeneralizedReedSolomonCode(F.list(), 13, yA)
        sage: B = codes.GeneralizedReedSolomonCode(F.list(), 5, yB)
        sage: C = codes.GeneralizedReedSolomonCode(F.list(), 17, yA.pairwise_product(yB))
        sage: A.schur_conductor(C) == LinearCode(B)
        True
    """
    return (self.schur_product(C.dual_code())).dual_code()

def hull(self):
    r"""
    Return the hull of `self`.

    TESTS::
        sage: from generic_code_operations import *
        sage: C = codes.HammingCode(GF(2), 3)
        sage: C.hull() == LinearCode(C.dual_code())
        True
    """
    return self.intersection(self.dual_code())

def invariant_code(self, permutation, check=False):
    r"""
    Return the invariant code of `self` with respect to `permutation`

    TESTS::
        sage: C = codes.HammingCode(GF(2), 3)
        sage: p = PermutationGroupElement([2,1,3,4,6,5,7])
        sage: p, C.is_permutation_automorphism(p)
        ((1,2)(5,6), True)
        sage: C.invariant_code(p).generator_matrix()
        [1 1 0 0 1 1 0]
        [0 0 1 0 1 1 0]
        [0 0 0 1 1 1 1]
    """
    perm = PermutationGroupElement(permutation)
    if (check and not(self.is_permutation_automorphism(perm))):
        raise ValueError("This permutation is not an automorphism of the code")
    P = perm.matrix() - identity_matrix(self.base_field(), self.length())
    H = P.stack(self.parity_check_matrix())
    return LinearCode(H.right_kernel_matrix())


AbstractLinearCode.vector_space = vector_space
AbstractLinearCode.__add__ = sum_of_codes
AbstractLinearCode.intersection = intersection_of_codes
AbstractLinearCode.base_extend = base_extend
AbstractLinearCode.concatenate = concatenate
AbstractLinearCode.schur_product = schur_product
AbstractLinearCode.schur_power = schur_power
AbstractLinearCode.schur_conductor = schur_conductor
AbstractLinearCode.hull = hull
AbstractLinearCode.invariant_code = invariant_code

