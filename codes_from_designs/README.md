# Codes from designs in **sagemath**

These files aim at handling _codes based on designs_ in **sagemath**.

### How to use it

Import paths and files. The path must be where you put your local `codes_from_designs` directory.
```python
import sys
sys.path.append('/path/to/your/folder/')
from codes_from_designs import *
```

You can build a code from one of the blocks designs existing in _SageMath_:
```python    
from sage.combinat.designs.block_design import BlockDesign
B = BlockDesign([[1,2],[1,3,5],[3,4,5]])
C1 = CodeFromDesign(B)
A = designs.AffineGeometryDesign(2, 1, 4)
C2 = CodeFromDesign(A, GF(2))
```

One can equip the code with a local decoder, using blocks as queries:
```python
A = designs.AffineGeometryDesign(2, 1, 4, point_coordinates=False)
C = CodeFromDesign(A)
Dec = DesignBasedLocalDecoder(C)
c = C.random_element()
assert all(Dec.locally_correct(c, i) == c[i] for i in range(C.length()))

Chan = channels.StaticErrorRateChannel(GF(2)**C.length(), 1)
y = Chan(c)
nb_failures = 0
nb_tests = 1000
for t in range(nb_tests):
    i = randint(0,C.length()-1)
    if (Dec.locally_correct(y, i) != c[i]):
        nb_failures += 1
print "error ratio = ", float(nb_failures)/nb_tests
```

Orthogonal arrays can be used to build transversal designs:
```python
from CodeFromDesign.oa_code import *
from CodeFromDesign.code_from_design import *
C = codes.GeneralizedReedSolomonCode(GF(8).list(), 2)
oa = OrthogonalArrayFromLinearCode(C, dual_min_distance=2, check=True)
D = DesignFromOrthogonalArray(oa)
C = CodeFromDesign(D)
assert (C.length() == 64 and C.dimension() == 37)
```