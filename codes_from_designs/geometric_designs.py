# -*- Python -*-
# -*- coding: utf-8 -*-


from sage.combinat.designs.block_design import BlockDesign
from sage.schemes.projective.projective_space import ProjectiveSpace
from sage.schemes.generic.hypersurface import ProjectiveHypersurface
from sage.combinat.integer_vector import IntegerVectors
from sage.misc.misc_c import prod

# --------------------------------------------------------------------------- #
# ---                        Helper functions                             --- #
def my_Pm(Fq, m):
    """ Returns the points in the projective space of degree m over Fq """
    if (m == 0):
        return [[Fq.one()]]
    infty = [ P + [Fq.zero()] for P in my_Pm(Fq, m-1) ]
    Am = [ list(P) + [Fq.one()] for P in Fq**m ]
    return infty + Am

def random_homogeneous_polynomial(R, deg):
    """ Returns a random m-variate homogeneous polynomial of degree deg """
    X = R.gens()
    Fq = R.base_ring()
    m = len(X)
    I = IntegerVectors(deg, m).list()
    N = len(I)
    C = [Fq.random_element() for i in range(N)]
    return sum([C[i] * prod([X[j]**I[i][j] for j in range(m)])
                for i in range(N)])
# --------------------------------------------------------------------------- #


# --------------------------------------------------------------------------- #
class HyperIntersectionDesign(BlockDesign):
    """
    Class for designs coming from intersections of an hypersurface with 
    the hyperplanes in a projective space.
    """
    def __init__(self, m, field, poly, algo="sage"):
        if (algo == "sage"):
            Pm = ProjectiveSpace(m, field)
            points = ProjectiveHypersurface(poly, Pm).rational_points()
            blocks = [ [ P for P in points
                         if sum([u[i]*P[i] for i in range(m+1)]) == 0 ]
                       for u in Pm ]
            super(HyperIntersectionDesign, self).__init__(
                points, blocks)
        elif (algo == "fast"):
            Pm = my_Pm(field, m)
            points = [ tuple(P) for P in Pm if poly(P) == 0 ]
            blocks = [ [ P for P in points
                         if sum([u[i]*P[i] for i in range(m+1)]) == 0 ]
                       for u in Pm ]
            super(HyperIntersectionDesign, self).__init__(
                points, blocks)
        else:
            raise ValueError("The algorithm must be \"sage\" or \"fast\"")
# --------------------------------------------------------------------------- #
