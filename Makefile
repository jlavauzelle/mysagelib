all: test

test:
	@echo "Testing codes_from_designs"
	(cd codes_from_designs; make test)
	@echo "Testing lifted_codes"
	(cd lifted_codes; make test)
	@echo "Testing code_constructions"
	(cd code_operations; make test)
	@echo "Testing group_codes"
	(cd group_codes; make test)

